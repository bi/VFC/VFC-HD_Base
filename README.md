# VFC-HD_Base

This is a template project for developments with the VFC-HD.

Tha application part of this project contains the instantiation of few example
status and control registers and an implementation of the DDR3 controller.

As of April 2021, the version of the Quartus tool supported is **20.1**. Using
a version other than this may result in problems with the IP cores. Note that
you **should not** update the IP version of the GBT-FPGA cores as they will
break.

## Creating your on project starting from this template

The first step to create a new project, in this example named **NewProject**,
based on this template is to clone this git and initialize its submodules. This
is done executing the following command:

```sh
git clone -–recursive https://gitlab.cern.ch/bi/VFC-HD_Base NewProject
```

The directory **NewProject** is created into your file system. The next step is
to link it to your desired remote, that here we assume to be a freshly created
repo in the **bi** group called **NewProject**.

```sh
git remote set-url origin https://gitlab.cern.ch/bi/NewProject
```

Now you can push into the new git to initialize it and start working:

```sh
git push
```

## Notes

This procedure unlinked your project from the base one but kept its history.
The submodules used as library, the [VFC-HD_System] and [BI_HDL_Cores], are
still kept as submodules and can be updated to the latest version at any
moment.

[VFC-HD_System]: https://gitlab.cern.ch/bi/VFC/VFC-HD_System
[BI_HDL_Cores]: https://gitlab.cern.ch/bi/BI_HDL_Cores

To update them:

```sh
cd ./libs/BI_HDL_Cores/
git checkout master
git pull

cd ./libs/VFC-HD_System/
git checkout master
git pull
```

