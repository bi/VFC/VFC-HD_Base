# Define here the paths used for the different folders:
quietly set VFC_HD_APP_PATH ../../../..
quietly set BI_HDL_CORES_PATH ../../../../../libs/BI_HDL_Cores/cores_for_synthesis
quietly set BI_HDL_CORES_SIM_PATH ../../../../../libs/BI_HDL_Cores/cores_for_simulation
quietly set VFC_DD3_PATH ../../../../../libs/vfc_ddr3
quietly set VFC_HD_SYS_PATH ../../../../../libs/VFC-HD_System/hdl
quietly set GBT_PATH $BI_HDL_CORES_PATH/bi_gbt_fpga
quietly set TESTBENCH_PATH ..
# TODO: get absolute path to the actual script automatically
quietly set THIS_SCRIPT ./compile.do

quietly set VERILOG_OPT "-suppress 2583"
    # vlog-2583: [SVCHK] - Some checking for conflicts with always_comb and always_latch variables not yet supported. Run vopt to provide additional design-level checks.
    # We are running vopt later, so we can supress this warning.
quietly set VHDL_OPT "-2008"
quietly set VHDL_MIXED_OPT "-2008 -mixedsvvh"
quietly set DEFAULT_LIB work
quietly set VSIM_OPT ""

# Clearing the transcript window:
.main clear

echo ""
echo "########################"
echo "# Starting Compilation #"
echo "########################"
quietly set CompilationStart [clock seconds]

###############################################
# Compile Processeses
###############################################

proc compile_file {type lib name {extraOptions ""}} {
    file stat $name result
    if {$::LastCompilationTime > $result(mtime)} {
        return;
    }

    if {[catch {
        switch -nocase $type {
            verilog {
                vlog {*}$::VERILOG_OPT {*}$extraOptions -work $lib $name
            }
            vhdl {
                vcom {*}$::VHDL_OPT {*}$extraOptions -work $lib $name
            }
            vhdl_mixed {
                vcom {*}$::VHDL_MIXED_OPT {*}$extraOptions -work $lib $name
            }
            copy {
                file copy -force $name .
            }
            default {
                return -code error "Unknown type '$type' of file '$name'."
            }
        }
    } errMsg opt]} {
        puts stderr "Error occurred when compiling files:"
        puts stderr "  $errMsg"
        return -options $opt $errMsg
    }
}

# another IP core compilation approach:
#  - use *.SPD files (Simulation Package Descriptor), can be found in most IP cores
#  - use ip-make-simscript.exe from c:\Programs\altera\15.1\quartus\sopc_builder\bin

# function to extract $IpCoreSimDir/mentor/msim_setup.tcl file
proc compile_ipcore {lib dir name} {
    return -code error "Deprecated. Try to use compileIpCore instead."
    # default paths for normal cores
    set IpCoreFile $dir/$name/$name.sip
    set IpCoreSimDir $dir/$name/$name\_sim
    if ![file exists $IpCoreFile] {
        # paths for QSys cores
        set IpCoreFile $dir/$name.qsys
        set IpCoreSimDir $dir/$name/simulation
    }
    file stat $IpCoreFile result
    if {$::LastCompilationTime < $result(mtime)} {
        set IpCoreScript $IpCoreSimDir/mentor/msim_setup.tcl
        set fp [open $IpCoreScript r]
        set FileData [read $fp]
        close $fp
        set FileLines [split $FileData "\n"]
        # compile files
        foreach Line $FileLines {
            if [regexp {(vlog|vcom) (-sv |())\$USER_DEFINED_COMPILE_OPTIONS\s+.*\"\$QSYS_SIMDIR(/[^\"]*)\"} $Line Match Command SvSwitch EmptyString File] {
                set FilePath $IpCoreSimDir$File
                if [string match "vcom" $Command] {
                    compile_file vhdl $lib $FilePath
                } else {
                    compile_file verilog $lib $FilePath
                }
            }
        }
        # copy files
        foreach Line $FileLines {
            if [regexp {file copy -force \$QSYS_SIMDIR(/.*) \./} $Line Match File] {
                set FilePath $IpCoreSimDir$File
                compile_file copy $lib $FilePath
            }
        }
    }
}

# fake function to extract IP library name and list of a simulation HDL files from SIP file
proc set_global_assignment args {
    upvar ipFiles ipFiles
    set nextArg ""
    set library ""
    set name ""
    set filePath ""
    set options ""
    # Parameter "options" is not official part of Quartus SIP. Hope he don't mind...
    foreach arg $args {
        if ![string equal $nextArg ""] {
            set $nextArg $arg
            set nextArg ""
        } elseif [string equal [string range $arg 0 0] "-"] {
            set nextArg [string range $arg 1 end]
        } else {
            set filePath $arg
        }
    }
    if [string equal $name "MISC_FILE"] {
        lappend ipFiles [list $library $filePath $options]
    }
}

proc extractIpFiles {sipFile} {
    set ::quartus(sip_path) [file dirname $sipFile]
    set ipFiles [list]
    source $sipFile
    return $ipFiles
}

# function to extract SIP file
proc compileIpCore {sipFile} {
    # We cannot skip this SIP file if no change was detected, as linked source
    # file can be changed.

    set ipFiles [extractIpFiles $sipFile]
    foreach file $ipFiles {
        set library [lindex $file 0]
        set filePath [lindex $file 1]
        set fileOptions [lindex $file 2]
        set dotPosition [string last "." $filePath]
        set extension [string range $filePath $dotPosition+1 end]

        set type unknown
        switch -nocase $extension {
            v -
            iv -
            vo -
            sv {
                set type verilog
            }
            vhd -
            vhdl {
                set type vhdl
            }
            hex {
                set type copy
            }
            sip {
                compileIpCore $filePath
                continue
            }
            c -
            h -
            tcl -
            qsys {
                # We know, that those files do not need to be processed.
                continue
            }
            default {
                return -code error "Unknown extension '$extension' of file '$filePath'."
            }
        }

        compile_file $type $library $filePath $fileOptions
    }
}

###############################################
# Defining Last Compilation
###############################################

echo ""
echo "-> Enabling Smart or Full Compilation..."
echo ""

# Checking whether a previous smart compilation exists:
quietly set no_sc 0;
if {![info exists LastCompilationTime]} {
    quietly set LastCompilationTime 0;
    quietly set no_sc 1;
    echo "No last compilation found."
}

# "clean" argument
if {($argc > 0 && [string equal $1 clean])} {
    quietly set LastCompilationTime 0;
    quietly set no_sc 1;
    echo "Cleaning and staring a new compilation..."
}

# Check whether this compile scripts is newer than last compilation, if yes, run full compilation
if {($argc > 0 && [string equal $1 ignore]) || [info exists IgnoreCompileScriptChange]} {
    echo "Possible compilation script change ignored."
} else {
    file stat $THIS_SCRIPT result
    if {$LastCompilationTime < $result(mtime)} {
        quietly set LastCompilationTime 0;
        quietly set no_sc 1;
        echo "Compilation script has changed."
    }
}

if "$no_sc == 1" {
    echo "Smart compilation not possible or not enabled. Running full compilation..."
    # Deleting pre-existing library
    if {[file exists $DEFAULT_LIB]} {vdel -all -lib $DEFAULT_LIB}
    # Creating working directory:
    vlib $DEFAULT_LIB
    quietly set no_sc 0;
} else {
    echo "Smart compilation enabled. Only out of date files will be compiled..."
    puts [clock format $LastCompilationTime -format {Previous compilation time: %A, %d of %B, %Y - %H:%M:%S}]
}

###############################################
# Compiling simulation files
###############################################

echo ""
echo "-> Starting Compilation..."
echo ""

# BI HDL CORES modules (synthesis):
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/ip_open_cores/generic_dpram_mod.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/ip_open_cores/generic_fifo_dc_gray_mod.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/Generic4InputRegs.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/Generic4OutputRegs.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/I2CMasterWb.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/SpiMasterWb.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/UniqueIdReader.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/Generic16InputRegs.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/Generic8InputRegs.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/StatusBusSynch.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/DpramGenericToWb.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/WbBus2M1S.v
compile_file vhdl    $DEFAULT_LIB $BI_HDL_CORES_PATH/SignalSyncer.vhd
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/avalon/t_AvalonMmInterface.sv
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/wishbone/t_WbInterface.sv
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/wishbone/WbAvalonMaster.sv
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/wishbone/WbCdc.sv
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/wishbone/WbDummy.sv
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_PATH/wishbone/WbPipelined2Standard.sv
compile_file verilog $DEFAULT_LIB $VFC_DD3_PATH/VfcDdr3.sv
compile_file verilog $DEFAULT_LIB $VFC_DD3_PATH/VfcDdr3WbToAvl.sv

# This is not working, because file path are too long (> 200 chars - Modelsim limit)
# and also order of VHDL files need to be adjusted.
# Simulation dummy files (see below) can be used to bypass GBT.
# # GBT
# # VfcHdGbtRefClk.qip
# compileIpCore        $GBT_PATH/gbt_fpga_vfc/vfc_hd_gbt_refclk/free_running_pll/FreeRunningPll.sip
# compile_file verilog $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/vfc_hd_gbt_refclk/AddrDecoderWbGbtRefClkSch.v
# compile_file verilog $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/vfc_hd_gbt_refclk/GbtRefClkScheme.sv
# compile_file verilog $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/vfc_hd_gbt_refclk/TxFrameClkBstSync.v
# compileIpCore        $GBT_PATH/gbt_fpga_vfc/../../ip_altera/alt_clk_ctrl/AltClkCtrl/simulation/AltClkCtrl.sip
# compile_file verilog $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/../../ip_altera/obuf_ddr/ObufDdr.v
# VfcHdGbtCoreX4.qip
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/alt_av_gbt_bank_package.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/alt_av_gbt_banks_user_setup.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_bank_package.vhd
# compileIpCore        $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/mgt/gx_std_x4/alt_av_gx_std_x4.sip
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/mgt/alt_av_mgt_std.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/mgt/multi_gigabit_transceivers.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/example_designs/altera_av/core_sources/alt_av_gbt_example_design.vhd
# compileIpCore        $GBT_PATH/gbt_fpga_vfc/example_designs/altera_av/core_sources/gbt_tx_frameclk_pll/gbt_tx_frameclk_pll.sip
# compileIpCore        $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/gbt_rx/alt_av_rx_dpram/alt_av_rx_dpram.sip
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/gbt_rx/alt_av_gbt_rx_gearbox_std_dpram.vhd
# compileIpCore        $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/gbt_tx/alt_av_tx_dpram/alt_av_tx_dpram.sip
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/gbt_tx/alt_av_gbt_tx_gearbox_std_dpram.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/mgt/alt_av_mgt_resetctrl.vhd
# compileIpCore        $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/mgt/gx_reset_rx/alt_av_gx_reset_rx.sip
# compileIpCore        $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/mgt/gx_reset_tx/alt_av_gx_reset_tx.sip
# compileIpCore        $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/mgt/mgt_reconfctrl_x4/alt_av_gx_reconfctrl_x4.sip
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/mgt/mgt_txpll/alt_av_mgt_txpll.vhd
# compileIpCore        $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/mgt/mgt_txpll/gx_reset_txpll/alt_av_gx_reset_txpll.sip
# compileIpCore        $GBT_PATH/gbt_fpga_vfc/gbt_bank/altera_av/mgt/mgt_txpll/gx_txpll/alt_av_gx_txpll.sip
# compile_file verilog $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/vfc_hd_gbt_core/AddrDecoderWbGbt.v
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/vfc_hd_gbt_core/alt_av_gbt_example_design_x4.vhd
# compile_file verilog $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/vfc_hd_gbt_core/GbtElinksDataAligner.sv
# compile_file verilog $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/vfc_hd_gbt_core/VfcHdGbtCoreX4.sv
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/example_designs/core_sources/clock_freq_meas/measure_refclk.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/example_designs/core_sources/rxframeclk_phalgnr/gbt_rx_frameclk_phalgnr.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/example_designs/core_sources/rxframeclk_phalgnr/phaligner_phase_comparator.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/example_designs/core_sources/rxframeclk_phalgnr/phaligner_phase_computing.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/example_designs/core_sources/gbt_bank_reset.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/sim_mod_files/gbt_pattern_checker_mod.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/sim_mod_files/gbt_pattern_generator_mod.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/example_designs/core_sources/gbt_pattern_matchflag.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/example_designs/core_sources/pattern_matchflag_delaymeas.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/mgt/mgt_latopt_bitslipctrl.vhd
# compile_file verilog $DEFAULT_LIB $GBT_PATH/../Generic8OutputRegs.v
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_chnsrch.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_deintlver.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_elpeval.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_errlcpoly.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_lmbddet.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_rs2errcor.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_rsdec.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_syndrom.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler_16bit.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler_21bit.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_framealigner.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_framealigner_bscounter.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_framealigner_pattsearch.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_framealigner_rightshift.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_framealigner_wraddr.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox_latopt.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox_std.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox_std_rdctrl.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx_status.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder_gbtframe_intlver.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder_gbtframe_polydiv.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder_gbtframe_rsencode.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_latopt.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_phasemon.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_std.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/sim_mod_files/gbt_tx_gearbox_std_rdwrctrl_mod.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler_16bit.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler_21bit.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_rx/gbt_rx.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_tx/gbt_tx.vhd
# compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_5_0_0/gbt_bank/core_sources/gbt_bank.vhd

# GBT simulation dummy
compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/sim_mod_files/gbt_pattern_generator_mod.vhd
compile_file vhdl    $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/sim_mod_files/gbt_pattern_checker_mod.vhd
compile_file verilog $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/simulation_dummy/VfcHdGbtCoreX4.sv
compile_file verilog $DEFAULT_LIB $GBT_PATH/gbt_fpga_vfc/simulation_dummy/GbtRefClkScheme.sv

# BI HDL CORES models (simulation):
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_SIM_PATH/I2CSlave.v
compile_file verilog $DEFAULT_LIB $BI_HDL_CORES_SIM_PATH/vme_bus_module/VmeBusModule.sv

# VFC_DDR3 library (simulation)
compileIpCore $VFC_DD3_PATH/VfcDdr3.sip

# VFC HD System modules (synthesis):
    # VFC-HD_System settings (needs to be set before including VfcHdSystem.sip)
    quietly set VFC_HD_CONF_DIR $VFC_HD_APP_PATH/modules
    # DDR3 settings
    quietly set DDR3_DATA_DIR data
    file mkdir $DDR3_DATA_DIR
    quietly lappend VSIM_OPT "+model_data+$DDR3_DATA_DIR"
    # choose one density according to which type of DDR3 chip is used on your board
    quietly set VFC_HD_DDR3_DENSITY den4096Mb
    # quietly set VFC_HD_DDR3_DENSITY den8192Mb
compileIpCore $VFC_HD_SYS_PATH/modules/VfcHdSystem.sip

# VFC HD Application modules (synthesis):
compile_file verilog $DEFAULT_LIB $VFC_HD_APP_PATH/modules/AddrDecoderWbApp.sv
compile_file verilog $DEFAULT_LIB $VFC_HD_APP_PATH/modules/VfcHdApplication.sv +incdir+$VFC_HD_SYS_PATH/modules/

# VFC HD Application models (simulation):
compile_file verilog $DEFAULT_LIB $VFC_HD_SYS_PATH/simulation/models/VfcHd_v3_0.sv +incdir+$VFC_HD_CONF_DIR

# Test Bench files:
compile_file verilog $DEFAULT_LIB $TESTBENCH_PATH/tb_BaseProject.sv

###############################################
# Top file
###############################################

echo ""
echo "-> Setting Top File..."
echo ""

quietly set top_level $DEFAULT_LIB.tb_BaseProject

###############################################
# Acquiring Compilation Time
###############################################

echo ""
quietly set duration [expr [clock seconds]-$CompilationStart]
quietly set LastCompilationTime $CompilationStart
echo [format "Compilation duration: %d:%02d" [expr $duration/60] [expr $duration%60]]
puts [clock format $LastCompilationTime -format {Setting last compilation time to: %A, %d of %B, %Y - %H:%M:%S}]

quietly set fp [open "LastCompilationTime.txt" w]
puts $fp $LastCompilationTime
close $fp

echo ""
echo "-> Compilation Done..."
echo ""
echo ""
