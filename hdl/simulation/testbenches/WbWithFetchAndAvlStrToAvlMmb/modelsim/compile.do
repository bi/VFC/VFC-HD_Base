quietly set VFC_HD_APP_PATH ../../../..
quietly set BI_HDL_CORES_PATH ../../../../../libs/BI_HDL_Cores/cores_for_synthesis
quietly set TESTBENCH_PATH ..

.main clear

vlog -work work $BI_HDL_CORES_PATH/wishbone/t_WbInterface.sv
vlog -work work $VFC_HD_APP_PATH/modules/CalControl.sv
vlog -work work $TESTBENCH_PATH/tb_CalControl.sv
