`timescale 1ns/100ps

module tb_WbWithFetchAndAvlStrToAvlMmb ();

// Clocking and resets
logic WbClk_k = 0, WbReset_r = 0;
logic AvlMMbClk_k = 0, AvlMMbReset_r = 0;
logic AvlStClk_k = 0;

always #10 WbClk_k     = ~WbClk_k;
always #4  AvlMMbClk_k = ~AvlMMbClk_k;
always #3  AvlStClk_k  = ~AvlStClk_k;

// Interfaves
t_WbInterface #(.g_DataWidth(32), .g_AddressWidth(6))
    Wb_tc(.Clk_k(WbClk_k), Reset_r.(WbReset_r));
t_AvalonSt_tt #(.g_DataWidth(128))
    AvlSt_tc(.Clk_k(AvlStClk_k));
t_AvalonMMb_tt #(.g_AddressWidth(26), .g_DataWidth(128), .g_BurstCountWidth(11))
    AvlMMb_tc(.Clk_k(AvlMMbClk_k), .Reset_r(AvlMMbReset_r));

//logic Declarations
logic LoadStreamerBaseAddress_p = 0;
logic [25:0] StreamerBaseAddress_b26  = 0,
             StreamerWriteAddress_b26 = 0;
logic [21:0] PageSelector_b22 = 0;
logic WriteFifoEmpty, PageFetched, FetchPage_p = 0;

// DUT
tb_WbWithFetchAndAvlStrToAvlMmb #( .g_Log2PageSize(6), .g_Log2BurstSize(10))
    i_DUT(
      .WbSlave_iot(Wb_tc),
      .PageSelector_ib(PageSelector_b22),
      .WriteFifoEmpty_o(WriteFifoEmpty),
      .FetchPage_ip(FetchPage_p),
      .PageFetched_o(PageFetched),
      .AvlStD128_ts(AvlSt_tc),
      .LoadStreamerBaseAddress_ip(LoadStreamerBaseAddress_p),
      .StreamerBaseAddress_ib26(StreamerBaseAddress_b26),
      .StreamerWriteAddress_ob26(StreamerWriteAddress_b26),
      .AvlMmBA26D128_tm(AvlMMb_tc));
// Test Logic
WbMasterSim i_WbMaster(
  .Rst_orq   (Wb_tc.Reset_r  ),
  .Clk_ik    (Wb_tc.Clk_k    ),
  .Adr_obq32 (Wb_tc.Adr_b    ),
  .Dat_obq32 (Wb_tc.DatMoSi_b),
  .Dat_ib32  (Wb_tc.DatMiSo_b),
  .We_oq     (Wb_tcWe.       ),
  .Cyc_oq    (Wb_tc.Cyc      ),
  .Stb_oq    (Wb_tc.Stb      ),
  .Ack_i     (Wb_tc.Ack      ));

logic [127:0] AvlMMbMemory_mb128 [2**26-1:0];

logic [10:0] BurstLnght_b11;
logic [25:0] Address_b26;
//write cycle

// checkers
ap_WriteStartNoRead: assert property ( @(posedge AvlMMbClk_k)
    $rose(AvlMMb_tc.WriteMoSi) |-> ~AvlMMb_tc.ReadMoSi
) else $error("AvalonMMb read and write cycles asserted at the same time");

ap_ReadStartNoWrite:assert property ( @(posedge AvlMMbClk_k)
    $rose(AvlMMb_tc.ReadMoSi) |-> ~AvlMMb_tc.WriteMoSi
) else $error("AvalonMMb read and write cycles asserted at the same time");

ap_AddressConstantWaitingGrant: assert endproperty ( @(posedge AvlMMbClk_k)
    ($rose(AvlMMb_tc.ReadMoSi)||$rose(AvlMMb_tc.WriteMoSi)) && AvlMMb_tc.WaitRequestMiSo
    |=>
    $stable(AvlMMb_tc.AddressMoSi_b)[*0:$] ##0 $fell(AvlMMb_tc.WaitRequestMiSo)
) else $error("Address changed waiting for access grant");

//write sequence
integer i = 0;

always @(posedge AvlMMbClk_k) if ($rose(AvlMMb_tc.WriteMoSi)) begin
    BurstLnght_b11 = AvlMMb_tc.BurstCountMoSi_b;
    Address_b26 = AvlMMb_tc.AddressMoSi_b;
    repeat (BurstLnght_b11) begin
        if (~AvlMMb_tc.WaitRequestMiSo) begin
            if (ByteEnableMoSi[0])
                AvlMMbMemory_mb128[Address_b26][7:0] = AvlMMb_tc.DataWriteMoSi_b[7:0];
                i=0;
                repeat(16) begin
                    if (ByteEnableMoSi[i])
                        AvlMMbMemory_mb128[Address_b26][8*(i+1)-1:8*i] = AvlMMb_tc.DataWriteMoSi_b8*(i+1)-1:8*i];
                    i=i+1;
                end
                Address_b26 = Address_b26 +1;
        end else begin
            @($fell(AvlMMb_tc.WaitRequestMiSo));
            repeat(16) begin
                if (ByteEnableMoSi[i])
                    AvlMMbMemory_mb128[Address_b26][8*(i+1)-1:8*i] = AvlMMb_tc.DataWriteMoSi_b8*(i+1)-1:8*i];
                i=i+1;
            end
            Address_b26 = Address_b26 +1;
        end
        @(posedge AvlMMbClk_k)
    end
end
//read sequence
always @(posedge AvlMMbClk_k) if ($rose(AvlMMb_tc.ReadMoSi)) begin
    BurstLnght_b11 = AvlMMb_tc.BurstCountMoSi_b;
    Address_b26 = AvlMMb_tc.AddressMoSi_b;
    repeat (BurstLnght_b11) begin
        if (~AvlMMb_tc.WaitRequestMiSo) begin
            AvlMMbMemory_mb128[Address_b26] = AvlMMb_tc.DataWriteMoSi_b;
            Address_b26 = Address_b26 +1;
        end else begin
            @($fell(AvlMMb_tc.WaitRequestMiSo));
            AvlMMbMemory_mb128[Address_b26] = AvlMMb_tc.DataWriteMoSi_b;
            Address_b26 = Address_b26 +1;
        end
        @(posedge AvlMMbClk_k)
    end
end

end
