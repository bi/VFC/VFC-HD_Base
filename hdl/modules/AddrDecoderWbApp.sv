`timescale 1ns/100ps

module AddrDecoderWbApp
(
    //==== WishBone Master's signals ====//
    input             Clk_ik,
    input      [20:0] Adr_ib21,
    output reg [20:0] Adr_oqb21,
    input      [31:0] DatMoSi_ib32,
    output reg [31:0] DatMoSi_oqb32,
    output reg [31:0] DatMiSo_oqb32,
    input             Stb_i,
    input             Cyc_i,
    output reg        Cyc_oq,
    input             We_i,
    output reg        We_oq,
    output reg        Ack_oq,

    //==== WishBone Slaves' signals ====//
    input      [31:0] DatCtrlReg_ib32,
    input             AckCtrlReg_i,
    output reg        StbCtrlReg_oq,
    input      [31:0] DatStatReg_ib32,
    input             AckStatReg_i,
    output reg        StbStatReg_oq,
    input      [31:0] DatEthSfpStatReg_ib32,
    input             AckEthSfpStatReg_i,
    output reg        StbEthSfpStatReg_oq,
    input      [31:0] DatI2cToWb_ib32,
    input             AckI2cToWb_i,
    output reg        StbI2cToWb_oq,
    input      [31:0] DatDdr3a_ib32,
    input             AckDdr3a_i,
    output reg        StbDdr3a_oq,
    input      [31:0] DatDdr3b_ib32,
    input             AckDdr3b_i,
    output reg        StbDdr3b_oq
);

//=======================================  Declarations  =====================================//

enum {
    c_SelNothing,
    c_SelCtrlReg,
    c_SelStatReg,
    c_SelEthSfpStatReg,
    c_SelI2cToWb,
    c_SelDdr3a,
    c_SelDdr3b
} SelectedModule;

//=======================================  User Logic  =======================================//

always @*
    casez(Adr_ib21)
        21'b0_0000_0000_0000_0000_00??: SelectedModule = c_SelCtrlReg;             // FROM 00_0000 TO 00_0003 (WB) == FROM 00_0000 TO 00_000C (VME) <-    4 regs ( 16B)
        21'b0_0000_0000_0000_0000_01??: SelectedModule = c_SelStatReg;             // FROM 00_0004 TO 00_0007 (WB) == FROM 00_0010 TO 00_001C (VME) <-    4 regs ( 16B)
        21'b0_0000_0000_0000_0000_10??: SelectedModule = c_SelEthSfpStatReg;       // FROM 00_0008 TO 00_000B (WB) == FROM 00_0020 TO 00_002C (VME) <-    4 regs ( 16B)
        21'b0_0000_0001_????_????_????: SelectedModule = c_SelI2cToWb;             // FROM 00_1000 TO 00_1FFF (WB) == FROM 00_4000 TO 00_7FFC (VME) <-   4k regs (16kB)
        21'b0_0000_0010_0???_????_????: SelectedModule = c_SelDdr3a;               // FROM 00_2000 TO 00_27FF (WB) == FROM 00_8000 TO 00_9FFC (VME) <-   2k regs (16kB)
        21'b0_0000_0010_1???_????_????: SelectedModule = c_SelDdr3b;               // FROM 00_2800 TO 00_2FFF (WB) == FROM 00_A000 TO 00_BFFC (VME) <-   2k regs (16kB)
        default:                        SelectedModule = c_SelNothing;
    endcase

always @(posedge Clk_ik) begin
    Ack_oq                                 <= #1  1'b0;
    DatMiSo_oqb32                          <= #1 32'h0;
    StbCtrlReg_oq                          <= #1  1'b0;
    StbStatReg_oq                          <= #1  1'b0;
    StbEthSfpStatReg_oq                    <= #1  1'b0;
    StbI2cToWb_oq                          <= #1  1'b0;
    StbDdr3a_oq                            <= #1  1'b0;
    StbDdr3b_oq                            <= #1  1'b0;
    case(SelectedModule)
        c_SelCtrlReg: begin
            StbCtrlReg_oq                  <= #1 Stb_i;
            DatMiSo_oqb32                  <= #1 DatCtrlReg_ib32;
            Ack_oq                         <= #1 AckCtrlReg_i;
        end
        c_SelStatReg: begin
            StbStatReg_oq                  <= #1 Stb_i;
            DatMiSo_oqb32                  <= #1 DatStatReg_ib32;
            Ack_oq                         <= #1 AckStatReg_i;
        end
        c_SelEthSfpStatReg: begin
            StbEthSfpStatReg_oq            <= #1 Stb_i;
            DatMiSo_oqb32                  <= #1 DatEthSfpStatReg_ib32;
            Ack_oq                         <= #1 AckEthSfpStatReg_i;
        end
        c_SelI2cToWb: begin
            StbI2cToWb_oq                  <= #1 Stb_i;
            DatMiSo_oqb32                  <= #1 DatI2cToWb_ib32;
            Ack_oq                         <= #1 AckI2cToWb_i;
        end
        c_SelDdr3a: begin
            StbDdr3a_oq                    <= #1 Stb_i;
            DatMiSo_oqb32                  <= #1 DatDdr3a_ib32;
            Ack_oq                         <= #1 AckDdr3a_i;
        end
        c_SelDdr3b: begin
            StbDdr3b_oq                    <= #1 Stb_i;
            DatMiSo_oqb32                  <= #1 DatDdr3b_ib32;
            Ack_oq                         <= #1 AckDdr3b_i;
        end
        c_SelNothing: begin
            DatMiSo_oqb32                  <= #1 32'hDEADBEEF;
            Ack_oq                         <= #1 Stb_i;
        end
    endcase
end

always @(posedge Clk_ik) Cyc_oq        <= #1 Cyc_i;
always @(posedge Clk_ik) We_oq         <= #1 We_i;
always @(posedge Clk_ik) Adr_oqb21     <= #1 Adr_ib21;
always @(posedge Clk_ik) DatMoSi_oqb32 <= #1 DatMoSi_ib32;

endmodule
