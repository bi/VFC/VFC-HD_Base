//----------------------------------------------------------------------
// Title      : VFC-HD Application Part
// Project    : VFC-HD_Base (https://gitlab.cern.ch/bi/VFC/VFC-HD_Base)
//----------------------------------------------------------------------
// File       : VfcHdApplication.sv
// Author     : -
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Application part of the VFC-HD gateware image
//----------------------------------------------------------------------

`timescale 1ns/100ps

module VfcHdApplication #(
    parameter g_ClkFrequency,
    parameter g_Synthesis = 1'b1,

    // Application release info, updated automatically by the
    // pre_flow.tcl script during implementation
    parameter g_ApplicationVersion_b8      = 8'h00,
              g_ApplicationReleaseDay_b8   = 8'h00,
              g_ApplicationReleaseMonth_b8 = 8'h00,
              g_ApplicationReleaseYear_b8  = 8'h00,

    // Application info string, up to 352 bits (44 ASCII characters)
    parameter g_AppIdent_b352 = "VFC-HD Base Project (DDR3 only)"
) (
    //------------------------------------------------------------------
    // External ports
    //------------------------------------------------------------------

    // IMPORTANT NOTE: Some ports must be specifically configured in
    // VfcHdConfig.vh in order to be enabled.

    // SFP MGT lanes (enable in VfcHdConfig.vh)
    input          BstSfpRx_i,
    output         BstSfpTx_o,

    input          EthSfpRx_i,
    output         EthSfpTx_o,

    input  [  4:1] AppSfpRx_ib4,
    output [  4:1] AppSfpTx_ob4,

    // DDR3 (enable in VfcHdConfig.vh)
    output         Ddr3aCk_ok,
    output         Ddr3aCk_okn,
    output         Ddr3aCke_o,
    output         Ddr3aReset_orn,
    output         Ddr3aRas_on,
    output         Ddr3aCas_on,
    output         Ddr3aCs_on,
    output         Ddr3aWe_on,
    output         Ddr3aOdt_o,
    output [  2:0] Ddr3aBa_ob3,
    output [ 15:0] Ddr3aAdr_ob16,
    output [  1:0] Ddr3aDm_ob2,
    inout  [  1:0] Ddr3aDqs_iob2,
    inout  [  1:0] Ddr3aDqs_iob2n,
    inout  [ 15:0] Ddr3aDq_iob16,

    output         Ddr3bCk_ok,
    output         Ddr3bCk_okn,
    output         Ddr3bCke_o,
    output         Ddr3bReset_orn,
    output         Ddr3bRas_on,
    output         Ddr3bCas_on,
    output         Ddr3bCs_on,
    output         Ddr3bWe_on,
    output         Ddr3bOdt_o,
    output [  2:0] Ddr3bBa_ob3,
    output [ 15:0] Ddr3bAdr_ob16,
    output [  1:0] Ddr3bDm_ob2,
    inout  [  1:0] Ddr3bDqs_iob2,
    inout  [  1:0] Ddr3bDqs_iob2n,
    inout  [ 15:0] Ddr3bDq_iob16,

    // Test I/O MMCX connectors
    inout          TestIo1_io,
    inout          TestIo2_io,

    // FMC HPC connector
    input          FmcPrsntM2c_in,
    input          FmcPgM2c_i,
    output         FmcPgC2m_o,
    output         FmcTck_ok,
    output         FmcTms_o,
    output         FmcTdi_o,
    input          FmcTdo_i,
    output         FmcTrstL_orn,
    inout          FmcScl_iok,
    inout          FmcSda_io,
    input          FmcClkDir_i,
    input          FmcClk0M2cCmos_ik,
    input          FmcClk1M2cCmos_ik,
    input          FmcClk2Bidir_iok,        // (enable and direction in VfcHdConfig.vh)
//  output         FmcClk2Bidir_iok,
    input          FmcClk3Bidir_iok,        // (enable and direction in VfcHdConfig.vh)
//  output         FmcClk3Bidir_iok,
    input          FmcGbtClk0M2cLeft_ik,    // (enable in VfcHdConfig.vh)
    input          FmcGbtClk1M2cLeft_ik,    // (enable in VfcHdConfig.vh)
    input          FmcGbtClk0M2cRight_ik,   // (enable in VfcHdConfig.vh)
    input          FmcGbtClk1M2cRight_ik,   // (enable in VfcHdConfig.vh)
    inout  [ 33:0] FmcLaP_iob34,
    inout  [ 33:0] FmcLaN_iob34,
    inout  [ 23:0] FmcHaP_iob24,
    inout  [ 23:0] FmcHaN_iob24,
    inout  [ 21:0] FmcHbP_iob22,
    inout  [ 21:0] FmcHbN_iob22,
    output [  9:0] FmcDpC2m_ob10,           // (enable in VfcHdConfig.vh)
    input  [  9:0] FmcDpM2c_ib10,           // (enable in VfcHdConfig.vh)

    // Clocks
    input          Si57xClk_ik,             // Si57X 10-160MHz programmable oscillator
    input  [  3:0] ClkFb_ikb4,              // Feedback clock inputs
    output [  3:0] ClkFb_okb4,              // Feedback clock outputs
    input          Clk20VCOx_ik,            //  20 MHz VCXO for WR
    input          GbitTrxClkRefR_ik,       // 125 MHz VCXO for WR (enable in VfcHdConfig.vh)
    output         PllSourceMuxOut_ok,      // Si5338 PLL input
    input          PllRefClkOut_ik,         // Si5338 PLL output (enable in VfcHdConfig.vh)

    // DACs for tuning WR VCXOs
    output         PllDac20Sync_o,
    output         PllDac25Sync_o,
    output         PllDacSclk_ok,
    output         PllDacDin_o,

    // Si5338 PLL I2C control
    inout          PllRefSda_ioz,
    inout          PllRefScl_iokz,
    input          PllRefInt_i,

    // P2 RTM (bit 0 is a clock capable input)
    inout  [ 19:0] P2DataP_iob20,
    inout  [ 19:0] P2DataN_iob20,

    // P0 timing
    input  [  7:0] P0HwHighByte_ib8,
    input  [  7:0] P0HwLowByte_ib8,
    output         DaisyChain1Cntrl_o,
    output         DaisyChain2Cntrl_o,
    input          VmeP0BunchClk_ik,
    input          VmeP0Tclk_ik,

    // GPIO LEMO connectors
    inout  [  4:1] GpIo_iob4,

    // WhiteRabbit PROM
    inout          WrPromSda_io,
    output         WrPromScl_ok,

    // Miscellaneous
    output         OeSi57x_oe,
    input  [  8:1] DipSw_ib8,
    input          PushButtonN_in,

    //------------------------------------------------------------------
    // System <=> Application interface
    //------------------------------------------------------------------

    // Clock & reset
    output         SysClk_ok,           // Clock for the system part
    output         SysReset_oar,        // Reset for the system part

    input          VmeSysReset_iarn,    // Asynchronous VME Sys Reset signal
    input          AppReset_iqr,        // Synchronous reset for the application

    // Application release ID and ident string
    output [ 31:0] AppRelease_ob32,
    output [351:0] AppIdent_ob352,

    // Vadj control
    output         VfmcEnable_oe,       // Request to enable the voltages on the FMC mezzanine
    input          VfmcEnabled_i,       // Confirmation of the enable state
    output [  2:0] VadjSelect_ob3,      // Values defined in VadjConfig.vh in the System library

    // WishBone interface
    input          WbMasterCyc_i,
    input          WbMasterStb_i,
    input  [ 24:0] WbMasterAdr_ib25,    // The actual width is 21 for the moment (top bits are 0)
    input          WbMasterWr_i,
    input  [ 31:0] WbMasterDat_ib32,
    output [ 31:0] WbMasterDat_ob32,
    output         WbMasterAck_o,
    output         WbSlaveCyc_o,
    output         WbSlaveStb_o,
    output [ 24:0] WbSlaveAdr_ob25,
    output         WbSlaveWr_o,
    output [ 31:0] WbSlaveDat_ob32,
    input  [ 31:0] WbSlaveDat_ib32,
    input          WbSlaveAck_i,

    // LEDs
    output [  8:1] Led_ob8,
    input  [  8:1] LedStatus_ib8,

    // BST interface                         _   _   _   _   _   _   _   _
    input          BstClk_ik,           // _/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_ ~160 MHz
    input          BstRst_ir,           //  :___    :        ___
    input          BunchClkFlag_i,      // _/   \___:___ ___/   \___ ___ ___
    input          BunchClkFlagDly_i,   // _:___ ___/   \___________/   \___ } ~40 MHz
    input          TurnClkFlag_i,       // _/   \___:____ __________________
    input          TurnClkFlagDly_i,    // _:_______/    \__________________ } ~11 kHz (LHC) / ~43 kHz (SPS)
    input  [  7:0] BstByteAddress_ib8,  //  <--Dly-->
    input  [  7:0] BstByteData_ib8,
    input          BstByteStrobe_i,
    input          BstByteError_i,

    // Interrupt request
    output [ 23:0] InterruptRequest_ob24,
    input  [ 23:0] InterruptAvailable_ib24,

    // GPIO direction (1=Output, 0=Input)
    output         GpIo1DirOut_o,
    input          GpIo1DirOutStatus_i,
    output         GpIo2DirOut_o,
    input          GpIo2DirOutStatus_i,
    output         GpIo34DirOut_o,
    input          GpIo34DirOutStatus_i,

    // GPIO termination (1=Enable, 0=Disable)
    output         GpIo1EnTerm_o,
    input          GpIo1EnTermStatus_i,
    output         GpIo2EnTerm_o,
    input          GpIo2EnTermStatus_i,
    output         GpIo3EnTerm_o,
    input          GpIo3EnTermStatus_i,
    output         GpIo4EnTerm_o,
    input          GpIo4EnTermStatus_i,

    // BLMIn P0 value
    input  [  7:0] BlmIn_ib8,

    // SFP control
    input          AppSfp1Present_iq,
    input          AppSfp1TxFault_iq,
    input          AppSfp1Los_iq,
    output         AppSfp1TxDisable_o,
    output         AppSfp1RateSelect_o,
    input          StatusAppSfp1TxDisable_iq,
    input          StatusAppSfp1RateSelect_iq,

    input          AppSfp2Present_iq,
    input          AppSfp2TxFault_iq,
    input          AppSfp2Los_iq,
    output         AppSfp2TxDisable_o,
    output         AppSfp2RateSelect_o,
    input          StatusAppSfp2TxDisable_iq,
    input          StatusAppSfp2RateSelect_iq,

    input          AppSfp3Present_iq,
    input          AppSfp3TxFault_iq,
    input          AppSfp3Los_iq,
    output         AppSfp3TxDisable_o,
    output         AppSfp3RateSelect_o,
    input          StatusAppSfp3TxDisable_iq,
    input          StatusAppSfp3RateSelect_iq,

    input          AppSfp4Present_iq,
    input          AppSfp4TxFault_iq,
    input          AppSfp4Los_iq,
    output         AppSfp4TxDisable_o,
    output         AppSfp4RateSelect_o,
    input          StatusAppSfp4TxDisable_iq,
    input          StatusAppSfp4RateSelect_iq,

    input          BstSfpPresent_iq,
    input          BstSfpTxFault_iq,
    input          BstSfpLos_iq,
    output         BstSfpTxDisable_o,
    output         BstSfpRateSelect_o,
    input          StatusBstSfpTxDisable_iq,
    input          StatusBstSfpRateSelect_iq,

    input          EthSfpPresent_iq,
    input          EthSfpTxFault_iq,
    input          EthSfpLos_iq,
    output         EthSfpTxDisable_o,
    output         EthSfpRateSelect_o,
    input          StatusEthSfpTxDisable_iq,
    input          StatusEthSfpRateSelect_iq,

    // CDR control
    input          CdrLos_iq,
    input          CdrLol_iq,

    // WishBone interface for the I2C slaves on the muxes
    output         I2cWbCyc_o,
    output         I2cWbStb_o,
    output         I2cWbWe_o,
    output  [11:0] I2cWbAdr_ob12,
    output  [ 7:0] I2cWbDat_ob8,
    input   [ 7:0] I2cWbDat_ib8,
    input          I2cWbAck_i,

    // OCT control sharing
    input   [15:0] OctSeriesControl_ib16,
    input   [15:0] OctParallelControl_ib16,

    // Diagnostics
    input   [63:0] UniqueId_ib64,
    input          UniqueIdValid_i,
    input   [15:0] Temp_ib16,
    input          TempRead_ip,
    input   [ 7:0] ChipTemp_ib8,
    input          ChipTempRead_ip,
    input   [11:0] Voltage0_ib12,   // PRE_VADJ
    input   [11:0] Voltage1_ib12,   // VCCPD_ADJ
    input   [11:0] Voltage2_ib12,   // VADJ
    input   [11:0] Voltage3_ib12,   // VIO_B_M2C
    input   [11:0] Voltage4_ib12,   // V3P3_FPGA
    input   [11:0] Voltage5_ib12,   // V3P3_FMC
    input   [11:0] Voltage6_ib12,   // V2P5
    input   [11:0] Voltage7_ib12,   // V12P0_FMC
    input   [ 7:0] VoltageOk_ib8,
    input          VoltageRead_ip,

    // VME geographical address
    input   [ 4:0] VmeGa_inb5,
    input          VmeGap_in
);

//===================================================================//
//                     VFC-HD VADJ CONSTANTS                         //
//===================================================================//

`include "VadjConfig.vh"

//===================================================================//
//                           DECLARATIONS                            //
//===================================================================//

genvar       i, j;

// Clocks & Reset
wire         pll_clk_100_k;
wire         pll_clk_125_k;
wire         a_Clk_k;
wire         pll_locked;
wire         VmeSysReset;

// LEDs
reg  [ 24:0] LedCounter_c25 = 0;
reg  [  8:1] Led_b8         = 8'h01;

// WishBone address decoder
wire         WbCyc, WbWe;
wire [ 20:0] WbAdr_b21;
wire [ 31:0] WbDatMoSi_b32;
wire         WbStbCtrlReg, WbAckCtrlReg;
wire [ 31:0] WbDatCtrlReg_b32;
wire         WbStbStatReg, WbAckStatReg;
wire [ 31:0] WbDatStatReg_b32;
wire         WbStbEthSfpStatReg, WbAckEthSfpStatReg;
wire [ 31:0] WbDatEthSfpStatReg_b32;
wire         WbMasterI2cStb, WbMasterI2cAck;
wire [  7:0] WbMasterI2cDat_b8;
wire         WbStbDdr3a, WbAckDdr3a;
wire [ 31:0] WbDatDdr3a_b32;
wire         WbStbDdr3b, WbAckDdr3b;
wire [ 31:0] WbDatDdr3b_b32;

// Control registers
wire [ 31:0] Ddr3CtrlReg0_b32;
wire [ 31:0] Ddr3CtrlReg1_b32;
wire [ 31:0] Ddr3CtrlReg2_b32;

// Status registers
logic [31:0] Ddr3StatusReg_b32;

// White Rabbit
wire         EthSfpIdValid;
wire [127:0] EthSfpPN_b128;
wire         EthSfpWbMasterCyc, EthSfpWbMasterStb, EthSfpWbMasterAck;
wire [ 11:0] EthSfpWbMasterAddr_b12;
wire [  7:0] EthSfpWbMasterDataMiSo_b8;

// Signals forwarding
reg          TestIo1;
reg          TestIo2;

// DDR3
logic [ 25:0] StdDdrIntAddress_b26 = 26'h3FF_FFFF;
logic [127:0] StdDdrIntData_b128;
wire          DdrInitAtLastAddress_a;
logic [ 15:0] StdDdrBytewe_b16 = 16'h0;
logic [ 15:0] StdDdraBytewe_b16, StdDdrbBytewe_b16;
logic         StdDdraReady, StdDdrbReady;
logic         Ddr3aPageFetched, Ddr3aInitDone, Ddr3aCalSuccess, Ddr3aCalFail;
logic         Ddr3bPageFetched, Ddr3bInitDone, Ddr3bCalSuccess, Ddr3bCalFail;

logic         AvlClk_k;
logic         Ddr3aAvlWaitRequest;
logic         Ddr3aAvlRead;
logic         Ddr3aAvlWrite;
logic  [10:0] Ddr3aAvlBurstCount_b11;
logic  [15:0] Ddr3aAvlByteEnable_b16;
logic  [25:0] Ddr3aAvlAddress_b26;
logic         Ddr3aAvlReadDataValid;
logic [127:0] Ddr3aAvlDataRead_b128;
logic [127:0] Ddr3aAvlDataWrite_b128;
logic         Ddr3bAvlWaitRequest;
logic         Ddr3bAvlRead;
logic         Ddr3bAvlWrite;
logic  [10:0] Ddr3bAvlBurstCount_b11;
logic  [15:0] Ddr3bAvlByteEnable_b16;
logic  [25:0] Ddr3bAvlAddress_b26;
logic         Ddr3bAvlReadDataValid;
logic [127:0] Ddr3bAvlDataRead_b128;
logic [127:0] Ddr3bAvlDataWrite_b128;

logic        a_FetchPage_p;         
logic [16:0] a_SelectedDdr3Page_b;  
logic        a_Ddr3bResetSoft_rn;   
logic        a_Ddr3bResetGlobal_rn; 
logic        a_Ddr3aResetSoft_rn;   
logic        a_Ddr3aResetGlobal_rn; 

logic        a_DdrInitMemory_p;     
logic        a_DdrInitMemorySel_p;  
logic [15:0] a_DdrByteWe_b16;       
logic [ 7:0] a_DdrInitByte_b8;      



//===================================================================//
//                              LOGIC                                //
//===================================================================//

//----------------------------------------------------------------------
// Fixed assignments
//----------------------------------------------------------------------

assign AppRelease_ob32 = {g_ApplicationReleaseYear_b8 [7:0],
                          g_ApplicationReleaseMonth_b8[7:0],
                          g_ApplicationReleaseDay_b8  [7:0],
                          g_ApplicationVersion_b8     [7:0]};
assign AppIdent_ob352  = {g_AppIdent_b352, {352-$size(g_AppIdent_b352){1'b0}}};
assign FmcPgC2m_o      = VfmcEnabled_i;
assign VadjSelect_ob3  = c_VadjSelect_V2P50; // Values defined in VadjConfig.vh
assign OeSi57x_oe      = 1'b1;

//----------------------------------------------------------------------
// Unused I/O
//----------------------------------------------------------------------

//Feedback clocks
assign ClkFb_okb4[0] = 1'b0;
assign ClkFb_okb4[1] = 1'b0;
assign ClkFb_okb4[3] = 1'b0;
//VME interrupts
assign InterruptRequest_ob24 = 24'b0;
//FMC
assign VfmcEnable_oe  = 1'b0;               // FMC voltages disabled
assign FmcDpC2m_ob10 = 10'b0;
assign FmcTck_ok = 1'b0;
assign FmcTms_o = 1'b0;
assign FmcTdi_o = 1'b0;
assign FmcTrstL_orn = 1'b1;
//Daisy chains
assign DaisyChain1Cntrl_o = 1'b0;
assign DaisyChain2Cntrl_o = 1'b0;
//WR PROM I2C
assign WrPromSda_io = 1'bz;
assign WrPromScl_ok = 0;
//External PLL Ref
assign PllRefSda_ioz         = 1'b0;
assign PllRefScl_iokz        = 1'b0;
assign PllSourceMuxOut_ok    = 1'b0;
//SFPs
assign AppSfp4TxDisable_o    = 1'b0;
assign AppSfp3TxDisable_o    = 1'b0;
assign AppSfp2TxDisable_o    = 1'b0;
assign AppSfp1TxDisable_o    = 1'b0;
assign AppSfp4RateSelect_o   = 1'b0;
assign AppSfp3RateSelect_o   = 1'b0;
assign AppSfp2RateSelect_o   = 1'b0;
assign AppSfp1RateSelect_o   = 1'b0;
assign BstSfpTxDisable_o     = 1'b0;
assign BstSfpRateSelect_o    = 1'b0;
assign EthSfpTxDisable_o     = 1'b0;
assign EthSfpRateSelect_o    = 1'b0;
assign AppSfpTx_ob4          = 4'b0;
assign BstSfpTx_o            = 1'b0;
assign EthSfpTx_o            = 1'b0;

//----------------------------------------------------------------------
// Clocks & Reset
//----------------------------------------------------------------------

// Reset Synchronization and Glitch Filter over 4 clock cycles
vme_reset_sync_and_filter i_vme_reset_sync_and_filter (
  .rst_ir   (1'b0),             
  .clk_ik   (Si57xClk_ik), //Main PLL ref clock
  .cen_ie   (1'b1),             
  .data_i   (!VmeSysReset_iarn),
  .data_o   (VmeSysReset)       
);

// PLL to generate local 100MHZ and 125MHz
// - The DDR3 requires the use of a 125MHz clock from FPGA logic
// - In case no DDR3 is used the SysClk can be directly an input 
//   pin like GbitTrxClkRefR_ik
 pll_100_125 i_pll_100_125(
    .refclk   (Si57xClk_ik),       //  refclk.clk
    .rst      (VmeSysReset),       //   reset.reset
    .outclk_0 (pll_clk_100_k),     // outclk0.clk
    .outclk_1 (pll_clk_125_k),     // outclk1.clk
    .locked   (pll_locked)         //  locked.export
);  

// Asynchronous reset for the system part, by default VmeSysReset.
// Note that AppReset_iqr provides a synchronised version of this reset
// for convenience.
assign SysReset_oar = !pll_locked;

// Clock for system part. The SYS_CLK_FREQ define in the VfcHdConfig.vh
// must be updated with the frequency of the clock supplied here.
assign SysClk_ok    = pll_clk_125_k;

//Application clock
assign a_Clk_k      = SysClk_ok;

//----------------------------------------------------------------------
// LEDs , GPIO and TestIO
//----------------------------------------------------------------------

always @(posedge a_Clk_k) LedCounter_c25 <= #1 LedCounter_c25 + 1'b1;
always @(posedge a_Clk_k) if (&LedCounter_c25) Led_b8 <= #1 {Led_b8[1], Led_b8[8:2]};
assign Led_ob8 = {Led_b8[8:5], Led_b8[1], Led_b8[2], Led_b8[3], Led_b8[4]};

always @* case(DipSw_ib8[2:1])
    2'h0: TestIo1 = BstClk_ik;
    2'h1: TestIo1 = BunchClkFlag_i;
    2'h2: TestIo1 = TurnClkFlag_i;
    2'h3: TestIo1 = a_Clk_k;
endcase
assign TestIo1_io = TestIo1;

always @* case(DipSw_ib8[4:3])
    2'h0: TestIo2 = GbitTrxClkRefR_ik;
    2'h1: TestIo2 = Si57xClk_ik;
    2'h2: TestIo2 = pll_clk_100_k;
    2'h3: TestIo2 = pll_clk_125_k;
endcase
assign TestIo2_io = TestIo2;

assign GpIo1DirOut_o  = 1'b0;   // input
assign GpIo2DirOut_o  = 1'b0;   // input
assign GpIo34DirOut_o = 1'b0;   // input
assign GpIo1EnTerm_o  = 1'b0;   // 50 ohm term. disabled
assign GpIo2EnTerm_o  = 1'b0;   // 50 ohm term. disabled
assign GpIo3EnTerm_o  = 1'b0;   // 50 ohm term. disabled
assign GpIo4EnTerm_o  = 1'b0;   // 50 ohm term. disabled
assign GpIo_iob4      = 4'bzzzz;


//------------------------------------------------------------------------
//-- Xtal DAC Control (125MHz VIN and 20MHz VCTRL)
//------------------------------------------------------------------------

spi_dac_osc #(
  .g_CLOCK_FREQ        (g_ClkFrequency)              // integer MHz
) i_spi_dac_osc(                            
  // Clock:                                    
  .clk_ik              (a_Clk_k),                    // in  std_logic;
  .rst_ir              (AppReset_iqr),               // in  std_logic;
  .cen_ie              (1'b1),                       // in  std_logic;
  //input val                             
  .new_val_i           (1'b0),                       // in std_logic;
  //0V0 is the midscale ( 20.000MHz) for the OSC3
  .dac20_val_i         (16'h0000),                   // in std_logic_vector(15 downto 0); --0x0000=0d00000=0V0 for midrange
  //1V6 is the midscale (125.000MHz) for the OSC2
  .dac25_val_i         (16'h51eb),                   // in std_logic_vector(15 downto 0); --0x51eb=0d20971=1V6 for midrange
  //SPI IO                           
  .PllDacSclk_ok       (PllDacSclk_ok),              // out std_logic;
  .PllDacDin_o         (PllDacDin_o),                // out std_logic;
  .PllDac25Sync_o      (PllDac25Sync_o),             // out std_logic;
  .PllDac20Sync_o      (PllDac20Sync_o)              // out std_logic
);

//----------------------------------------------------------------------
// Boot ROM
//----------------------------------------------------------------------

// Perform selection between BOOT ROM master and WB master.
// The BOOT ROM master has precedence on the App WB during boot, then is fully disconnected.
// Configurable timeout inside of the BOOT ROM ensures that it does not stall the bus indefinitely.

// Define the following constant to enable the BOOT ROM functionality.
// Path is relative to your Quartus project directory.
//`define APP_BOOT_ROM "../mif/BootRom.mif"

`ifdef APP_BOOT_ROM

`ifndef APP_BOOT_ROM_MAX_TIME
`define APP_BOOT_ROM_MAX_TIME 3000000
`endif

`ifndef APP_BOOT_ROM_MAX_WAIT
`define APP_BOOT_ROM_MAX_WAIT 10000
`endif

// BOOT ROM WbMaster
BootRomWbMaster #(
    .g_ClkFrequency             (g_ClkFrequency),
    .g_InitFile                 (`APP_BOOT_ROM),
    .p_MaxBootTimeMicrosec      (`APP_BOOT_ROM_MAX_TIME), // maximum overall time, in us, default 3s
    .p_MaxAckWaitTimeMicrosec   (`APP_BOOT_ROM_MAX_WAIT)  // maximum ack wait time for each command, us, 10ms
) i_BootRomWbMaster (
    .Clk_ik     (a_Clk_k),
    .Rst_irq    (AppReset_iqr),     // Reset (initiate) on application reset release
    .SelBoot_oq (),                 // Bus takeover flag (if other master are used)
    .Cyc_oq     (WbSlaveCyc_o),
    .Stb_oq     (WbSlaveStb_o),
    .We_oq      (WbSlaveWr_o),
    .Adr_oqb22  (WbSlaveAdr_ob25[21:0]),
    .Dat_oqb32  (WbSlaveDat_ob32),
    .Ack_i      (WbSlaveAck_i));
    
assign WbSlaveAdr_ob25[24:22]  = 3'h0; //Address MSB are not used
//WbSlaveDat_ib32;    // Return data not used by the BOOT ROM
`else

//----------------------------------------------------------------------
// WB Slave to the System Block
//----------------------------------------------------------------------
// Direct connection to System master if BOOT ROM disabled
// To be changed by the user if needed
assign WbSlaveCyc_o     =  1'b0;
assign WbSlaveStb_o     =  1'b0;
assign WbSlaveAdr_ob25  = 25'h0;
assign WbSlaveWr_o      =  1'b0;
assign WbSlaveDat_ob32  = 32'h0;
// WbSlaveAck_i;
// WbSlaveDat_ib32;
`endif

//----------------------------------------------------------------------
// WishBone address decoder
//----------------------------------------------------------------------

AddrDecoderWbApp i_AddrDecoderWbApp (
    .Clk_ik                           (a_Clk_k),
    .Adr_ib21                         (WbMasterAdr_ib25[20:0]),
    .Adr_oqb21                        (WbAdr_b21),
    .DatMoSi_ib32                     (WbMasterDat_ib32),
    .DatMoSi_oqb32                    (WbDatMoSi_b32),
    .DatMiSo_oqb32                    (WbMasterDat_ob32),
    .Stb_i                            (WbMasterStb_i),
    .Cyc_i                            (WbMasterCyc_i),
    .Cyc_oq                           (WbCyc),
    .We_i                             (WbMasterWr_i),
    .We_oq                            (WbWe),
    .Ack_oq                           (WbMasterAck_o),

    .DatCtrlReg_ib32                  (WbDatCtrlReg_b32),
    .AckCtrlReg_i                     (WbAckCtrlReg),
    .StbCtrlReg_oq                    (WbStbCtrlReg),

    .DatStatReg_ib32                  (WbDatStatReg_b32),
    .AckStatReg_i                     (WbAckStatReg),
    .StbStatReg_oq                    (WbStbStatReg),

    .DatEthSfpStatReg_ib32            (WbDatEthSfpStatReg_b32),
    .AckEthSfpStatReg_i               (WbAckEthSfpStatReg),
    .StbEthSfpStatReg_oq              (WbStbEthSfpStatReg),

    .DatI2cToWb_ib32                  ({24'h0, WbMasterI2cDat_b8}),
    .AckI2cToWb_i                     (WbMasterI2cAck),
    .StbI2cToWb_oq                    (WbMasterI2cStb),

    .DatDdr3a_ib32                    (WbDatDdr3a_b32),
    .AckDdr3a_i                       (WbAckDdr3a),
    .StbDdr3a_oq                      (WbStbDdr3a),

    .DatDdr3b_ib32                    (WbDatDdr3b_b32),
    .AckDdr3b_i                       (WbAckDdr3b),
    .StbDdr3b_oq                      (WbStbDdr3b)
);

//----------------------------------------------------------------------
// DDR3 interface
//----------------------------------------------------------------------

// Control and Status registers 
Generic4OutputRegs #(
    .Reg0Default     ( 32'hBABE0000 ),
    .Reg0AutoClrMask ( 32'hFFFFFFFF ),
    .Reg1Default     ( 32'h00000000 ),
    .Reg1AutoClrMask ( 32'h7FFFFFFF ),
    .Reg2Default     ( 32'h00000000 ),
    .Reg2AutoClrMask ( 32'h7FFFFFFF ),
    .Reg3Default     ( 32'h00000000 ),
    .Reg3AutoClrMask ( 32'hFFFFFFFF ))
i_ControlRegs ( 
    .Clk_ik          ( a_Clk_k           ),
    .Rst_irq         ( AppReset_iqr      ),
    .Cyc_i           ( WbCyc             ),
    .Stb_i           ( WbStbCtrlReg      ),
    .We_i            ( WbWe              ),
    .Adr_ib2         ( WbAdr_b21[1:0]    ),
    .Dat_ib32        ( WbDatMoSi_b32     ),
    .Dat_oab32       ( WbDatCtrlReg_b32  ),
    .Ack_oa          ( WbAckCtrlReg      ),
    .Reg0Value_ob32  ( Ddr3CtrlReg0_b32  ),
    .Reg1Value_ob32  ( Ddr3CtrlReg1_b32  ),
    .Reg2Value_ob32  ( Ddr3CtrlReg2_b32  ),
    .Reg3Value_ob32  ( /* unconnected */ ));

assign a_FetchPage_p           =  Ddr3CtrlReg1_b32[    31 ];
assign a_SelectedDdr3Page_b    =  Ddr3CtrlReg1_b32[ 16: 0 ];
assign a_Ddr3bResetSoft_rn     = ~Ddr3CtrlReg0_b32[     3 ];
assign a_Ddr3bResetGlobal_rn   = ~Ddr3CtrlReg0_b32[     2 ];
assign a_Ddr3aResetSoft_rn     = ~Ddr3CtrlReg0_b32[     1 ];
assign a_Ddr3aResetGlobal_rn   = ~Ddr3CtrlReg0_b32[     0 ];
assign a_DdrInitMemory_p       =  Ddr3CtrlReg2_b32[    31 ];
assign a_DdrInitMemorySel_p    =  Ddr3CtrlReg2_b32[    30 ]; // 0 for mem A, 1 for mem B
assign a_DdrByteWe_b16         =  Ddr3CtrlReg2_b32[ 23: 8 ];
assign a_DdrInitByte_b8        =  Ddr3CtrlReg2_b32[  7: 0 ];

Generic4InputRegs i_StatusRegs (
    .Clk_ik         ( a_Clk_k           ),
    .Rst_irq        ( AppReset_iqr      ),
    .Cyc_i          ( WbCyc             ),
    .Stb_i          ( WbStbStatReg      ),
    .Adr_ib2        ( WbAdr_b21[1:0]    ),
    .Dat_oab32      ( WbDatStatReg_b32  ),
    .Ack_oa         ( WbAckStatReg      ),
    .Reg0Value_ib32 ( Ddr3StatusReg_b32 ),
    .Reg1Value_ib32 ( 32'hACDCDEAD      ),
    .Reg2Value_ib32 ( 32'hBEEFCAFE      ),
    .Reg3Value_ib32 ( 32'h0             ));     

assign Ddr3StatusReg_b32[ 31: 8 ] = 24'b0;
assign Ddr3StatusReg_b32[     7 ] = Ddr3bPageFetched;
assign Ddr3StatusReg_b32[     6 ] = Ddr3bCalFail;
assign Ddr3StatusReg_b32[     5 ] = Ddr3bCalSuccess;
assign Ddr3StatusReg_b32[     4 ] = Ddr3bInitDone;
assign Ddr3StatusReg_b32[     3 ] = Ddr3aPageFetched;
assign Ddr3StatusReg_b32[     2 ] = Ddr3aCalFail;
assign Ddr3StatusReg_b32[     1 ] = Ddr3aCalSuccess;
assign Ddr3StatusReg_b32[     0 ] = Ddr3aInitDone;

// DDR initialization (write control) logic
always @(posedge a_Clk_k) begin
    if (a_DdrInitMemory_p)
        StdDdrIntAddress_b26 <= 26'h0;
    else if (~DdrInitAtLastAddress_a)
        StdDdrIntAddress_b26 <= StdDdrIntAddress_b26 + (a_DdrInitMemorySel_p? StdDdrbReady : StdDdraReady);
    if (~DdrInitAtLastAddress_a || a_DdrInitMemory_p) begin
        if (|a_DdrByteWe_b16) begin
            StdDdrBytewe_b16   <= a_DdrByteWe_b16;
            StdDdrIntData_b128 <= {16{a_DdrInitByte_b8}};
        end else begin // Counter init
            StdDdrBytewe_b16   <= 16'hFFFF;
            StdDdrIntData_b128 <= a_DdrInitMemory_p ? 128'h0 : StdDdrIntData_b128 + (a_DdrInitMemorySel_p? StdDdrbReady : StdDdraReady);
        end
    end else begin
        StdDdrBytewe_b16   <= 16'h0;
        StdDdrIntData_b128 <= 128'h0;
    end
end

assign DdrInitAtLastAddress_a = &StdDdrIntAddress_b26;
assign StdDdraBytewe_b16 = a_DdrInitMemorySel_p ? 16'b0 : StdDdrBytewe_b16;
assign StdDdrbBytewe_b16 = a_DdrInitMemorySel_p ? StdDdrBytewe_b16 : 16'b0;

// DDR example interface instantiation
VfcDdr3Interface #(
        .g_Log2PageSize              ( 11 ),
        .g_Log2StdInterfaceFifoDepth (  4 ))
i_VfcDdr3aInterface (
    //WB interface (read port)
    .WbClk_ik           ( a_Clk_k                ),
    .WbCyc_i            ( WbCyc                  ),
    .WbStb_i            ( WbStbDdr3a             ),
    .WbAdr_ib           ( WbAdr_b21[10:0]        ),
    .WbDat_ob32         ( WbDatDdr3a_b32         ),
    .WbAck_o            ( WbAckDdr3a             ),
    //Paging controls
    .PageSelector_ib    ( a_SelectedDdr3Page_b   ),
    .FetchPage_ip       ( a_FetchPage_p          ),
    .PageFetched_o      ( Ddr3aPageFetched       ),
    //'Stdandard' memory interface (write port)
    .StdClk_ik          ( a_Clk_k                ),
    .StdAddress_ib26    ( StdDdrIntAddress_b26   ),
    .StdByteWe_ib16     ( StdDdraBytewe_b16      ),
    .StdData_ib128      ( StdDdrIntData_b128     ),
    .StdWriteReady_o    ( StdDdraReady           ),
    //Avalon interface to the DDR core
    .AvlClk_ik          ( AvlClk_k               ),
    .AvlWaitRequest_i   ( Ddr3aAvlWaitRequest    ),
    .AvlRead_o          ( Ddr3aAvlRead           ),
    .AvlWrite_o         ( Ddr3aAvlWrite          ),
    .AvlBurstCount_ob11 ( Ddr3aAvlBurstCount_b11 ),
    .AvlByteEnable_ob16 ( Ddr3aAvlByteEnable_b16 ),
    .AvlAddress_ob26    ( Ddr3aAvlAddress_b26    ),
    .AvlReadDataValid_i ( Ddr3aAvlReadDataValid  ),
    .AvlDataRead_ib128  ( Ddr3aAvlDataRead_b128  ),
    .AvlDataWrite_ob128 ( Ddr3aAvlDataWrite_b128 ));

VfcDdr3Interface #(
    .g_Log2PageSize              ( 11 ),
    .g_Log2StdInterfaceFifoDepth (  4 ))
i_VfcDdr3bInterface (
    //WB interface (read port)
    .WbClk_ik           ( a_Clk_k                ),
    .WbCyc_i            ( WbCyc                  ),
    .WbStb_i            ( WbStbDdr3b             ),
    .WbAdr_ib           ( WbAdr_b21[10:0]        ),
    .WbDat_ob32         ( WbDatDdr3b_b32         ),
    .WbAck_o            ( WbAckDdr3b             ),
    //Paging controls
    .PageSelector_ib    ( a_SelectedDdr3Page_b   ),
    .FetchPage_ip       ( a_FetchPage_p          ),
    .PageFetched_o      ( Ddr3bPageFetched       ),    
    //'Stdandard' memory interface (write port)
    .StdClk_ik          ( a_Clk_k                ),
    .StdAddress_ib26    ( StdDdrIntAddress_b26   ),
    .StdByteWe_ib16     ( StdDdrbBytewe_b16      ),
    .StdData_ib128      ( StdDdrIntData_b128     ),
    .StdWriteReady_o    ( StdDdrbReady           ),
    //Avalon interface to the DDR core
    .AvlClk_ik          ( AvlClk_k               ),
    .AvlWaitRequest_i   ( Ddr3bAvlWaitRequest    ),
    .AvlRead_o          ( Ddr3bAvlRead           ),
    .AvlWrite_o         ( Ddr3bAvlWrite          ),
    .AvlBurstCount_ob11 ( Ddr3bAvlBurstCount_b11 ),
    .AvlByteEnable_ob16 ( Ddr3bAvlByteEnable_b16 ),
    .AvlAddress_ob26    ( Ddr3bAvlAddress_b26    ),
    .AvlReadDataValid_i ( Ddr3bAvlReadDataValid  ),
    .AvlDataRead_ib128  ( Ddr3bAvlDataRead_b128  ),
    .AvlDataWrite_ob128 ( Ddr3bAvlDataWrite_b128 ));

//DDR3 core(s)
VfcDdr3 i_VfcDdr3(
    .AvlClk_ok              ( AvlClk_k               ),
    //DDR3 A Avalon interface
    .Ddr3aAvlAddress_ib26   ( Ddr3aAvlAddress_b26    ),
    .Ddr3aAvlReadDataValid_o( Ddr3aAvlReadDataValid  ),
    .Ddr3aAvlDataRead_ob128 ( Ddr3aAvlDataRead_b128  ),
    .Ddr3aAvlDataWrite_ib128( Ddr3aAvlDataWrite_b128 ),
    .Ddr3aAvlByteEnable_ib16( Ddr3aAvlByteEnable_b16 ),
    .Ddr3aAvlRead_i         ( Ddr3aAvlRead           ),
    .Ddr3aAvlWrite_i        ( Ddr3aAvlWrite          ),
    .Ddr3aAvlBurstCount_ib11( Ddr3aAvlBurstCount_b11 ),
    .Ddr3aAvlWaitRequest_o  ( Ddr3aAvlWaitRequest    ),
    //DDR3 B Avalon interface
    .Ddr3bAvlAddress_ib26   ( Ddr3bAvlAddress_b26    ),
    .Ddr3bAvlReadDataValid_o( Ddr3bAvlReadDataValid  ),
    .Ddr3bAvlDataRead_ob128 ( Ddr3bAvlDataRead_b128  ),
    .Ddr3bAvlDataWrite_ib128( Ddr3bAvlDataWrite_b128 ),
    .Ddr3bAvlByteEnable_ib16( Ddr3bAvlByteEnable_b16 ),
    .Ddr3bAvlRead_i         ( Ddr3bAvlRead           ),
    .Ddr3bAvlWrite_i        ( Ddr3bAvlWrite          ),
    .Ddr3bAvlBurstCount_ib11( Ddr3bAvlBurstCount_b11 ),
    .Ddr3bAvlWaitRequest_o  ( Ddr3bAvlWaitRequest    ),
    //controller clock and resets
    .RefClk125Mhz_ik        ( ClkFb_ikb4[2]          ),
    .Ddr3aResetGlobal_irn   ( a_Ddr3aResetGlobal_rn  ),
    .Ddr3aResetSoft_irn     ( a_Ddr3aResetSoft_rn    ),
    .Ddr3bResetGlobal_irn   ( a_Ddr3bResetGlobal_rn  ),
    .Ddr3bResetSoft_irn     ( a_Ddr3bResetSoft_rn    ),
    // controller status
    .Ddr3aInitDone_o        ( Ddr3aInitDone          ),
    .Ddr3aCalSuccess_o      ( Ddr3aCalSuccess        ),
    .Ddr3aCalFail_o         ( Ddr3aCalFail           ),
    .Ddr3bInitDone_o        ( Ddr3bInitDone          ),
    .Ddr3bCalSuccess_o      ( Ddr3bCalSuccess        ),
    .Ddr3bCalFail_o         ( Ddr3bCalFail           ),
    //direct connections to the DDR3 chips and to the OCT control
    .*); 
    
// DDR3 PLL reference clock
//   A transceiver reference clock cannot be directly routed to the DDR3 PLL reference clock
//   It is fed back externally via a clock input pin
assign ClkFb_okb4[2]  = a_Clk_k; //125MHz clock tree from PLL (do not connect directly GbitTrxClkRefR_ik)

//----------------------------------------------------------------------
// White Rabbit
//----------------------------------------------------------------------

// Note!! The White Rabbit interface is still to be added...

// Ethernet SFP ID reader
SfpIdReader #(
    .g_SfpWbBaseAddress (16'ha00),
    .g_WbAddrWidth      (     12))
i_EthSfpVendorIdReader (
    .Clk_ik        (a_Clk_k),
    .SfpPlugged_i  (EthSfpPresent_iq),
    .SfpIdValid_o  (EthSfpIdValid),
    .SfpPN_ob128   (EthSfpPN_b128),
    .WbCyc_o       (EthSfpWbMasterCyc),
    .WbStb_o       (EthSfpWbMasterStb),
    .WbAddr_ob     (EthSfpWbMasterAddr_b12),
    .WbData_ib8    (EthSfpWbMasterDataMiSo_b8),
    .WbAck_i       (EthSfpWbMasterAck));

// Dual master arbiter for the I2C slaves on the I2C muxes
WbBus2M1S #(
    .g_DataWidth ( 8),
    .g_AddrWidth (12))
i_WbBus2M1S(
    .Clk_ik      (a_Clk_k),
    .CycM1_i     (EthSfpWbMasterCyc),
    .StbM1_i     (EthSfpWbMasterStb),
    .WeM1_i      (1'b0),
    .AddrM1_ib   (EthSfpWbMasterAddr_b12),
    .DataM1_ib   (/* Not connected */),
    .AckM1_o     (EthSfpWbMasterAck),
    .DataM1_ob   (EthSfpWbMasterDataMiSo_b8),
    .CycM2_i     (WbCyc),
    .StbM2_i     (WbMasterI2cStb),
    .WeM2_i      (WbWe),
    .AddrM2_ib   (WbAdr_b21[11:0]),
    .DataM2_ib   (WbDatMoSi_b32[ 7:0]),
    .AckM2_o     (WbMasterI2cAck),
    .DataM2_ob   (WbMasterI2cDat_b8),
    .CycS_o      (I2cWbCyc_o),
    .StbS_o      (I2cWbStb_o),
    .WeS_o       (I2cWbWe_o),
    .AddrS_ob    (I2cWbAdr_ob12),
    .DataS_ob    (I2cWbDat_ob8),
    .AckS_i      (I2cWbAck_i),
    .DataS_ib    (I2cWbDat_ib8));

// Ethernet SFP status registers bank
Generic4InputRegs i_EthSfpStatusRegs (
    .Clk_ik         (a_Clk_k),
    .Rst_irq        (AppReset_iqr),
    .Cyc_i          (WbCyc),
    .Stb_i          (WbStbEthSfpStatReg),
    .Adr_ib2        (WbAdr_b21[1:0]),
    .Dat_oab32      (WbDatEthSfpStatReg_b32),
    .Ack_oa         (WbAckEthSfpStatReg),
    //--
    .Reg0Value_ib32 (EthSfpPN_b128[ 31: 0]),
    .Reg1Value_ib32 (EthSfpPN_b128[ 63:32]),
    .Reg2Value_ib32 (EthSfpPN_b128[ 95:64]),
    .Reg3Value_ib32 (EthSfpPN_b128[127:96]));



endmodule
