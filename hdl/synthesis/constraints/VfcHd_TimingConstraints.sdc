# ##########################################################
#   System timing constraints for VFC-HD
# ##########################################################

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Do not edit this part unless you are completely sure you know what you are doing.
# If you need to add/modify some constraints concerning the Application part, please do it in
#   the "Application timing constraints" section bellow.
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# ----------------------------------------------------------
# Time Information
# ----------------------------------------------------------
#set_time_format -unit ns -decimal_places 3

# ----------------------------------------------------------
#   Create Clocks
# ----------------------------------------------------------

# 125 MHz VCTCXO Osc3
create_clock -name cc_GbitTrxClkRefR -period 8ns     [get_ports {GbitTrxClkRefR_ik}]

# 100 MHz clock from OSC1
create_clock -name cc_Si57xClk       -period 10ns    [get_ports {Si57xClk_ik}]

# 16 MHz VME clock
create_clock -name cc_VmeSysClock    -period 62.5ns  [get_ports {VmeSysClk_ik}]

# 20 MHz clock from OSC3
create_clock -name cc_Clk20VCOx      -period 50ns    [get_ports {Clk20VCOx_ik}]

# 160 MHz from BST
create_clock -name cc_CdrClkOut      -period 6.25ns  [get_ports {CdrClkOut_ik}]

# 120 MHz Differential reference clock for the MGT lines
# create_clock -name cc_PllRefClkOut   -period 8.333ns [get_ports {PllRefClkOut_ik}]

# ----------------------------------------------------------
# Create Generated Clock
# ----------------------------------------------------------

# Derive the remaining constraint automatically for generated clocks (PLLs, ...) 
# => Must be placed after all the "create_generated_clock" of all SDC files
derive_pll_clocks

# Transfer the pin names to more readable variables
set cc_pll_clk_100 { i_VfcHdApplication|i_pll_100_125|pll_100_125_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk }
set cc_pll_clk_125 { i_VfcHdApplication|i_pll_100_125|pll_100_125_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk }
puts "PLL CLK 100: $cc_pll_clk_100"
puts "PLL CLK 125: $cc_pll_clk_125"

# System Clock
set cc_SystemClock $cc_pll_clk_125
puts "SYS CLK: $cc_SystemClock"

# ----------------------------------------------------------
# Set Clock Latency
# ----------------------------------------------------------

# ----------------------------------------------------------
# Set Clock Uncertainty
# ----------------------------------------------------------
# Derive the clock uncertainty
derive_clock_uncertainty

# ----------------------------------------------------------
#   Clock Groups
# ----------------------------------------------------------
#https://www.intel.com/content/www/us/en/programmable/support/support-resources/design-examples/design-software/timinganalyzer/clocking/tq-clock-groups.html
#The following example shows a set_clock_groups command and the equivalent set_false_path commands.
# Clocks A & C are never active when clocks B & D are active
#     set_clock_groups -exclusive -group {A C} -group {B D}
# Equivalent specification using false paths
#     set_false_path -from [get_clocks A] -to [get_clocks B]
#     set_false_path -from [get_clocks A] -to [get_clocks D]
#     set_false_path -from [get_clocks C] -to [get_clocks B]
#     set_false_path -from [get_clocks C] -to [get_clocks D]
#     set_false_path -from [get_clocks B] -to [get_clocks A]
#     set_false_path -from [get_clocks B] -to [get_clocks C]
#     set_false_path -from [get_clocks D] -to [get_clocks A]
#     set_false_path -from [get_clocks D] -to [get_clocks C]
# -asynchronous: Asynchronous clocks are those that are completely unrelated (e.g., have different ideal clock sources)
# -exclusive   : Exclusive clocks are not actively used in the design at the same time (e.g., multiplexed clocks)

# Unrelated asynchronous clock groups
set_clock_groups -asynchronous -group {cc_GbitTrxClkRefR}\
                               -group {cc_Si57xClk}\
                               -group {cc_VmeSysClock}\
                               -group {cc_Clk20VCOx}\
                               -group {cc_CdrClkOut}\
                               -group [get_clocks {altera_reserved_tck}]\
                               -group [get_clocks $cc_pll_clk_100]\
                               -group [get_clocks $cc_pll_clk_125]
                               # -group {cc_PllRefClkOut}\

# ----------------------------------------------------------
#   Asynchronous Connections
# ----------------------------------------------------------
# https://www.intel.com/content/www/us/en/programmable/support/support-resources/design-examples/design-software/timinganalyzer/tq-false-path.html
# set_false_path allows to remove specific constraints between clocks or specific signals, with many options. 
# For example setup checks while keeping hold checks or select only one edge (rise or fall):
#   set_false_path -rise_from CLKA -fall_to CLKB -setup
# For entire clock domains,set_clock_groups is faster and simpler. 

# VME
set_false_path -to   [get_ports {VmeADir_o}]
set_false_path -to   [get_ports {VmeA_iob31[*]}]
set_false_path -to   [get_ports {VmeDDir_o}]
set_false_path -to   [get_ports {VmeD_iob32[*]}]
set_false_path -to   [get_ports {VmeDtAckOe_o}]
set_false_path -to   [get_ports {VmeIackOut_on}]
set_false_path -to   [get_ports {VmeIrq_ob7[*]}]
set_false_path -to   [get_ports {VmeLWord_ion}]
set_false_path -to   [get_ports {altera_reserved_tdo}]
set_false_path -from [get_ports {VmeA_iob31[*]}]
set_false_path -from [get_ports {VmeAm_ib6[*]}]
set_false_path -from [get_ports {VmeAs_in}]
set_false_path -from [get_ports {VmeD_iob32[*]}]
set_false_path -from [get_ports {VmeDs_inb2[*]}]
set_false_path -from [get_ports {VmeIackIn_in}]
set_false_path -from [get_ports {VmeIack_in}]
set_false_path -from [get_ports {VmeLWord_ion}]
set_false_path -from [get_ports {VmeSysReset_irn}]
set_false_path -from [get_ports {VmeWrite_in}]
set_false_path -from [get_ports {altera_reserved_tdi}]
set_false_path -from [get_ports {altera_reserved_tms}]

# Misc System I/O
set_false_path -from [get_ports {I2CIoExpIntApp12_in}]
set_false_path -from [get_ports {I2CIoExpIntApp34_in}]
set_false_path -from [get_ports {I2CIoExpIntBlmIn_in}]
set_false_path -from [get_ports {I2CIoExpIntBstEth_in}]
set_false_path -from [get_ports {I2CIoExpIntLos_in}]
set_false_path -from [get_ports {PcbRev_ib7[*]}]
set_false_path -from [get_ports {PushButtonN_in}]
set_false_path -from [get_ports {DipSw_ib8[*]}]
set_false_path -to   [get_ports {ResetFpgaConfigN_orn}]
set_false_path -from [get_ports {FmcPgM2c_i}]
set_false_path -from [get_ports {FmcPrsntM2c_in}]
set_false_path -to   [get_ports {FmcPgC2m_o}]
set_false_path -to   [get_ports {VfmcEnable_oen}]

# ----------------------------------------------------------
#   Input/Output Delays
# ----------------------------------------------------------
# https://www.intel.com/content/dam/altera-www/global/en_US/uploads/b/bd/Timing_Analysis_Workshop.pptx
# min input delay  ~= min clk to output time of external chip + min board delay - max clock skew
# max input delay  ~= max clk to output time of external chip + max board delay - min clock skew
# min output delay ~= hold time of external chip
# max output delay ~= setup time of external chip

# SPI FMC Voltage Control
# https://datasheets.maximintegrated.com/en/ds/MAX5481-MAX5484.pdf
# sclk_period   Tclkper: tCP= 140ns min (7MHz|142ns)
# cs_to_clk     Tsetup : tCSS= 60ns min
# din_to_clk    Tsetup : tDS=  40ns min
# din_after_clk Thold  : tDH=  0ns  min
set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VadjSclk_ok}]
create_clock -name cc_VadjSclk -period 142ns  [get_ports {VadjSclk_ok}]
set_clock_groups -asynchronous -group [get_clocks $cc_SystemClock] -group {cc_VadjSclk}
# - min out time = tDH          => 0ns 
# - max out time = tCP/2 - tCSS => 10ns
set_output_delay -clock cc_VadjSclk -min  0.0 [get_ports {VadjCs_o}]
set_output_delay -clock cc_VadjSclk -max 10.0 [get_ports {VadjCs_o}]
set_output_delay -clock cc_VadjSclk -min  0.0 [get_ports {VadjDin_o}]
set_output_delay -clock cc_VadjSclk -max 10.0 [get_ports {VadjDin_o}]

# SPI ADC Voltage monitoring
# https://www.analog.com/media/en/technical-documentation/data-sheets/AD7888.pdf
# sclk_period   Tclkper: t6+t7= 200ns min (2MHz|500ns)
# cs_to_clk     Tsetup : t1=     10ns min
# din_to_clk    Tsetup : t4=     20ns min
# din_after_clk Thold  : t5=     20ns min
# clk_to_out    Tout   : t3=     75ns min / 100ns max
set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VAdcSclk_ok}]
create_clock -name cc_VAdcSclk -period 500ns  [get_ports {VAdcSclk_ok}]
set_clock_groups -asynchronous -group [get_clocks $cc_SystemClock] -group {cc_VAdcSclk}
# - min in time = (Tclkper)/2 - Tout_max => 0ns
# - max in time = (Tclkper)/2 - Tout_min => 25ns
set_input_delay  -clock cc_VAdcSclk -min  0.0 [get_ports {VAdcDout_i}]
set_input_delay  -clock cc_VAdcSclk -max 25.0 [get_ports {VAdcDout_i}]
# - min out time = Thold                 => 0ns
# - max out time = (Tclkper)/2 - Tsetup  => 10ns
set_output_delay -clock cc_VAdcSclk -min  0.0 [get_ports {VAdcCs_o}]
set_output_delay -clock cc_VAdcSclk -max 10.0 [get_ports {VAdcCs_o}]
set_output_delay -clock cc_VAdcSclk -min  0.0 [get_ports {VAdcDin_o}]
set_output_delay -clock cc_VAdcSclk -max 10.0 [get_ports {VAdcDin_o}]

# I2C Mux & IO expanders
# https://www.ti.com/lit/gpn/PCA9534
# sclk_period   Tclkper: tsch+tscl= 500+500ns min (1MHz|1us)
# din_to_clk    Tsetup : tsds=      100ns min
# din_after_clk Thold  : tsdh=        0ns min
# clk_to_out    Tout   : tvd=         0ns min / 300ns max
set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {I2cMuxScl_iokz}]
set_input_delay  -clock [get_clocks $cc_SystemClock] 0 [get_ports {I2cMuxScl_iokz}]
create_clock -name cc_I2cMuxScl -period 1000ns  [get_ports {I2cMuxScl_iokz}]
set_clock_groups -asynchronous -group [get_clocks $cc_SystemClock] -group {cc_I2cMuxScl}
# - min in time = tlow - Tout_max => 0ns
# - max in time = tlow - Tout_min => 200ns
set_input_delay  -clock cc_I2cMuxScl -min   0.0 [get_ports {I2cMuxSda_ioz}]
set_input_delay  -clock cc_I2cMuxScl -max 200.0 [get_ports {I2cMuxSda_ioz}]
# - min out time = Thold           => 0ns
# - max out time = thigh - Tsetup  => 50ns
set_output_delay -clock cc_I2cMuxScl -min   0.0 [get_ports {I2cMuxSda_ioz}]
set_output_delay -clock cc_I2cMuxScl -max 400.0 [get_ports {I2cMuxSda_ioz}]

# I2C WR PROM
# http://ww1.microchip.com/downloads/en/DeviceDoc/21189T.pdf
# sclk_period   Tclkper: tHIGH+tLOW= 600+1300ns min (400kHz|2.5us)
# din_to_clk    Tsetup : tSU=             100ns min
# din_after_clk Thold  : tHD=               0ns min
# clk_to_out    Tout   : tAA=             300ns min / 1300ns max
set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {WrPromScl_ok}]
create_clock -name cc_WrPromScl -period 50ns  [get_ports {WrPromScl_ok}]
set_clock_groups -asynchronous -group [get_clocks $cc_SystemClock] -group {cc_WrPromScl}
# - min out time = Thold          => 0ns
# - max out time = thigh - Tsetup => 200ns
set_output_delay -clock cc_WrPromScl -min   0.0 [get_ports {WrPromSda_io}]
set_output_delay -clock cc_WrPromScl -max 200.0 [get_ports {WrPromSda_io}]
# - min in time = tlow - Tout_max => 0ns
# - max in time = tlow - Tout_min => 1000ns
set_input_delay  -clock cc_WrPromScl -min    0.0 [get_ports {WrPromSda_io}]
set_input_delay  -clock cc_WrPromScl -max 1000.0 [get_ports {WrPromSda_io}]

# OneWire temperature and ID (very slow bus, no constraint)
# https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf
# write_0       Twr0: TLOW0= 60us min
# write_1       Twr1: TLOW1=  1us min
# read_data_vld Tout: tRDV=  15us max
# - very slow bus: no timing constraint
set_false_path -from [get_ports {TempIdDq_ioz}]
set_false_path -to   [get_ports {TempIdDq_ioz}]

# BST
set_input_delay  -clock [get_clocks {cc_CdrClkOut}] -clock_fall 0 [get_ports {CdrDataOut_i}]
#TODO input         BstDataIn_i,


# ##########################################################
#   Application timing constraints
# ##########################################################

# Here you can add/modify constraints concerning the Application part

# ----------------------------------------------------------
#   Create Clocks
# ----------------------------------------------------------

# Application Clock
set cc_ApplicationClock $cc_SystemClock
puts "APP CLK: $cc_ApplicationClock"

# The DDR3 Clocks are already defined in the DDR3 SDC which must be called before this SDC in the QSF
puts "DDR3 clocks"
puts "PLL REF CLK: $pll_ref_clock"
puts "AFI CLK:     $local_pll_afi_clk"
puts "AFI PHY CLK: $local_pll_afi_phy_clk"
puts "DQ WR CLK:   $local_pll_dq_write_clk"
puts "WR CLK:      $local_pll_write_clk"
puts "ADDR CLK:    $local_pll_addr_cmd_clk"
puts "AVL CLK:     $local_pll_avl_clock"
puts "CONF CLK:    $local_pll_config_clock"
puts "HR CLK:      $local_pll_hr_clock"
puts "SMPL CLK:    $local_sampling_clock"

# ----------------------------------------------------------
# Create Generated Clock
# ----------------------------------------------------------


# ----------------------------------------------------------
#   Clock Groups
# ----------------------------------------------------------

# The application clock is asynchronous with the DDR3 core Avalon clock AFI (Arria PHY Interface EMIF Clock)
set_clock_groups -asynchronous -group [get_clocks $cc_ApplicationClock] -group $local_pll_afi_clk

# ----------------------------------------------------------
#   Asynchronous Connections
# ----------------------------------------------------------

# DDR3
# To remove the warning 332009 betwwen the application clock and the DDR3 AVL and config clocks
#   - The launch and latch times for the relationship between clocks are outside of the legal time range. 
# https://www.intel.com/content/www/us/en/programmable/support/support-resources/knowledge-base/solutions/rd11302014_394.html
set_false_path -from [get_clocks $cc_ApplicationClock] -to $local_pll_avl_clock
set_false_path -from [get_clocks $cc_ApplicationClock] -to $local_pll_config_clock

# The application DDR3 reset register and the DDR3 reset inputs are asynchronous but stable signals
set_false_path -from [get_registers i_VfcHdApplication|i_ControlRegs|Reg_q4b32[0][*]]

# The application DDR3 status register and the DDR3 calibration status are asynchronous but stable
set_false_path -to [get_registers i_VfcHdApplication|i_StatusRegs|Dat_oab32[*]]

# Clock inputs
set_false_path -from [get_ports {CdrClkOut_ik}]
set_false_path -from [get_ports {Clk20VCOx_ik}]
set_false_path -from [get_ports {GbitTrxClkRefR_ik}]
set_false_path -from [get_ports {Si57xClk_ik}]

# # I2C External Reference PLL
# # https://static6.arrow.com/aropdfconversion/427ecce88aa2d51553d4ee0d05c80a3b3f89b4fc/272416091631965si5338.pdf
# # sclk_period   Tclkper: tHIGH+tLOW= 600+1300ns min (400kHz|2.5us)
# # din_to_clk    Tsetup : tSU=             600ns min
# # din_after_clk Thold  : tHD=               0ns min
# # clk_to_out    Tout   : tAA=             900ns min / 1300ns max
# create_clock -name cc_PllRefScl -period 2500ns [get_ports {PllRefScl_iokz}]
# set_output_delay -clock [get_clocks $cc_ApplicationClock] 0 [get_ports {PllRefScl_iokz}]
# set_input_delay  -clock [get_clocks $cc_ApplicationClock] 0 [get_ports {PllRefScl_iokz}]
# set_clock_groups -asynchronous -group [get_clocks $cc_ApplicationClock] -group {cc_PllRefScl}
# # - min out time = Thold          => 600ns
# # - max out time = thigh - Tsetup => 700ns
# set_output_delay -clock cc_PllRefScl -min     0 [get_ports {PllRefSda_ioz}]
# set_output_delay -clock cc_PllRefScl -max 700.0 [get_ports {PllRefSda_ioz}]
# # - min in time = tlow - Tout_max => 0ns
# # - max in time = tlow - Tout_min => 400ns
# set_input_delay  -clock cc_PllRefScl -min   0.0 [get_ports {PllRefSda_ioz}]
# set_input_delay  -clock cc_PllRefScl -max 400.0 [get_ports {PllRefSda_ioz}]
set_false_path -from [get_ports {PllRefScl_iokz}]
set_false_path -from [get_ports {PllRefSda_ioz}]
set_false_path -to   [get_ports {PllRefScl_iokz}]
set_false_path -to   [get_ports {PllRefSda_ioz}]

# Misc Application inputs
set_false_path -from [get_ports {ClkFb_ikb4[0]}]
# set_false_path -from [get_ports {ClkFb_ikb4[2]}] DDR3 reference clock
set_false_path -from [get_ports {ClkFb_ikb4[1]}]
set_false_path -from [get_ports {ClkFb_ikb4[3]}]

# Misc Application outputs
set_false_path -to   [get_ports {ClkFb_okb4[*]}]
set_false_path -to   [get_ports {TestIo1_io}]
set_false_path -to   [get_ports {TestIo2_io}]
set_false_path -to   [get_ports {VmeLWord_ion}]

# ----------------------------------------------------------
#   Input/Output Delays
# ----------------------------------------------------------

# SPI Oscillator DACs
# https://www.analog.com/media/en/technical-documentation/data-sheets/AD5620_5640_5660.pdf
# sclk_period   Tclkper: t1= 50ns min (20MHz|50ns)
# din_to_clk    Tsetup : t5=  5ns min
# din_after_clk Thold  : t6=  4.5ns min
create_clock -name cc_PllDacSclk -period 50ns  [get_ports {PllDacSclk_ok}]
set_output_delay -clock [get_clocks $cc_ApplicationClock] 0 [get_ports {PllDacSclk_ok}]
set_clock_groups -asynchronous -group [get_clocks $cc_ApplicationClock] -group {cc_PllDacSclk}
# - min out time = Thold                => 4.5ns
# - max out time = (Tclkper)/2 - Tsetup => 20ns
set_output_delay -clock cc_PllDacSclk -min  4.5 [get_ports {PllDacDin_o}]
set_output_delay -clock cc_PllDacSclk -max 20.0 [get_ports {PllDacDin_o}]
set_output_delay -clock cc_PllDacSclk -min  4.5 [get_ports {PllDac25Sync_o}]
set_output_delay -clock cc_PllDacSclk -max 20.0 [get_ports {PllDac25Sync_o}]
set_output_delay -clock cc_PllDacSclk -min  4.5 [get_ports {PllDac20Sync_o}]
set_output_delay -clock cc_PllDacSclk -max 20.0 [get_ports {PllDac20Sync_o}]
