class Ddr3ControllerWithFetch:
    def __init__(self, VfcClassObject):
        self.Bus = VfcClassObject
        print "DDR3 instantiated with default interface register mapping: modify if needed"
        self.Log2PageSize      = 11
        self.DDR3AddressBits   = 27
        self.GlobalResetReg    = "ExampleControlReg"
        self.GlobalResetBit    = 0
        self.GlobalResetPol    = 1
        self.SoftResetReg      = "ExampleControlReg"
        self.SoftResetBit      = 1
        self.SoftResetPol      = 1
        self.InitDoneReg       = "ExampleStatusReg"
        self.InitDoneBit       = 0
        self.CalSuccessReg     = "ExampleStatusReg"
        self.CalSuccessBit     = 1
        self.CalFailReg        = "ExampleStatusReg"
        self.CalFailBit        = 2
        self.PageSelReg        = "app_ddr3_page_selector"
        self.PageSelFirstBit   = 0
        self.FetchPageReg      = "app_ddr3_page_selector"
        self.FetchPageBit      = 31
        self.DDR3InitReg      = "app_ddr3_init_control"
        self.DDR3InitBit       = 31
        self.WriteFifoEmptyReg = "ExampleStatusReg"
        self.WriteFifoEmptyBit = 8
        self.PageFetchDoneReg  = "ExampleStatusReg"
        self.PageFetchDoneBit  = 3
        self.PageMem           = "app_ddr3_master"

    def SoftReset(self):
        if (self.SoftResetPol==1):
            self.Bus.SetBit(self.SoftResetReg, self.SoftResetBit)
        else:
            self.Bus.ClearBit(self.SoftResetReg, self.SoftResetBit)
        self.Bus.ToggleBit(self.SoftResetReg, self.SoftResetBit)

    def GlobalReset(self):
       if (self.GlobalResetPol==1):
           self.Bus.SetBit(self.GlobalResetReg, self.GlobalResetBit)
       else:
           self.Bus.ClearBit(self.GlobalResetReg, self.GlobalResetBit)
       self.Bus.ToggleBit(self.GlobalResetReg, self.GlobalResetBit)

    def IsInitDone(self):
        return self.Bus.IsBitSet(self.InitDoneReg, self.InitDoneBit)

    def IsCalFail(self):
        return self.Bus.IsBitSet(self.CalFailReg, self.CalFailBit)

    def IsCalSuccess(self):
        return self.Bus.IsBitSet(self.CalSuccessReg, self.CalSuccessBit)

    def InitDdr3(self):
        self.SoftReset()
        self.GlobalReset()
        while not (self.IsInitDone() or self.IsCalFail() or self.IsCalSuccess()):
            pass
        if (self.IsInitDone() and self.IsCalSuccess()):
            return True
        else:
            return False

    def IsPageFetched(self):
        return self.Bus.IsBitSet(self.PageFetchDoneReg, self.PageFetchDoneBit)

    def SetPage(self, Page):
        PageBitsNumber = self.DDR3AddressBits - self.Log2PageSize
        self.Bus.WriteRegSlice(self.PageSelReg, Page, self.PageSelFirstBit, PageBitsNumber)

    def FetchPage(self, Page):
        self.SetPage(Page)
        self.Bus.SetBit(self.FetchPageReg, self.FetchPageBit)
        self.Bus.ClearBit(self.FetchPageReg, self.FetchPageBit)
        while (not self.IsPageFetched()):
            pass

    def ReadPage(self, Page, LwToRead=0):
        self.FetchPage(Page)
        if LwToRead==0:
            LwToRead = 2**self.Log2PageSize
        return self.Bus.ReadMem(self.PageMem, LwToRead)

    def Read(self, Address):
        self.FetchPage(Address/(2**self.Log2PageSize))
        return self.Bus.Read(self.PageMem, Address%(2**self.Log2PageSize))

    def Write(self, Data, Address):
        self.SetPage(Address/(2**self.Log2PageSize))
        return self.Bus.Write(self.PageMem, Data, Address%(2**self.Log2PageSize))

    def TestDataBits(self):
        TestOk = True
        for i in range(0,16):
            self.Write(2**i, i)
        Data = self.ReadPage(0, 16)
        for i in range(0,16):
            if Data[i]!=2**i :
                TestOk = False
                print("ERROR -> Data bit %d not working: read %x instead of %x" % (i, Data[i], 2**i))
        return TestOk

    def TestAddressBits(self):
        TestOk = True
        for i in range(0,self.DDR3AddressBits):
            self.Write(i, 2**i)
        self.Write(0, 0)
        for i in range(0,self.DDR3AddressBits):
            Data = self.Read(2**i)
            if Data!=i:
                TestOk = False
                print("ERROR -> Address bit %d not working: read %x instead of %x" % (i, Data, i))
        return TestOk

    def DdrStdPortTest(self):
       self.Bus.Write(self.DDR3InitReg, 0x4000000A)
       self.Bus.SetBit(self.DDR3InitReg, self.DDR3InitBit)
       #self.Bus.ClearBit(self.DDR3InitReg, self.DDR3InitBit)
       page = self.ReadPage(0)
       print page
       page = self.ReadPage(1)
       print page
       page = self.ReadPage(2)
       print page
