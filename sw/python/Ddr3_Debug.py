## Importing application specific VFC-HD class:
from libs.Class_VfcHd_Base import *
from libs.CommonFunctions import *
import random
import numpy as np
from time import sleep

## Opening VME connection with VFC-HD:
Args      = sys.argv
ArgLength = len(Args)
Verbose = False
if ArgLength == 2:
    Lun   = int(Args[1], 10)
    VfcHd = VfcHd_Base(Lun, 3, 164, "ROAK", 200, Verbose)
else:
    print
    print "Error!! LUN must be provided..."
    print
    sys.exit()


ReadMemBuffer  = []
WriteMemBuffer = []

VfcHd.WhoAreYou()

print "-> Testing DDR3:"
print "-> -------------"
print "->"
print "-> Initializing the module"
print "-> Initialization of the DDR3 B module success: ", VfcHd.Ddr3B.InitDdr3()

VfcHd.Ddr3B.Bus.Write(VfcHd.Ddr3B.DDR3InitReg, 0x80000000)
for Val in  np.array(VfcHd.Ddr3B.ReadPage(0, 20*4)).reshape(20,4):
	print '%08x' % Val[0]
